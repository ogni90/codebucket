export class CodeBucketError extends Error {
  constructor(message: string) {
    super(message);
  }
}
