import { IssueTracker } from './issuetracker';

export interface JiraConfiguration {
  host: string;
  projectKeys: string[];
}

export class JiraIssueTracker implements IssueTracker {

  constructor(
    private readonly cfg: JiraConfiguration) {
  }

  public findIssueUrl(message: string): string | undefined {
    for (const projectKey of this.cfg.projectKeys) {
      const match = message.match(`\\b(${projectKey.toUpperCase()}-\\d+)\\b`);
      if (match) {
        return `https://${this.cfg.host}browse/${match[1]}`;
      }
    }
  }

}
