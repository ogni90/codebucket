# Change Log

## [Unreleased]

## [0.2.0]

* Feature: New support for Bitbucket Server
* Fix: Incorrect word boundary handling in Bitbucket issue detection
* Fix: Regular expression encoding

## [0.1.1]

* Initial release